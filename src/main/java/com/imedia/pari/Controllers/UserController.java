package com.imedia.pari.Controllers;

import com.imedia.pari.Models.Response.UserResponse;
import com.imedia.pari.Models.User;
import com.imedia.pari.Repository.UserRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "application/json")
    public Object CreateUser(@RequestBody String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String name = jsonObject.getString("name");
            String numero = jsonObject.getString("numero");
            String photo = null;
            String defaultCode = "0000";
            long initSolde = 0;
            if (jsonObject.has("photo")) {
                 photo = jsonObject.getString("photo");
            }
            User user = new User(name,numero,initSolde,defaultCode,photo);
            userRepository.save(user);

            return new UserResponse(1,false,"user has been create successfully",userRepository.findByNumero(numero));
        } catch (JSONException e) {
            e.printStackTrace();
        }
            return new UserResponse(0,true,"failed to create User",null);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = "application/json")
    public Object UpdateUser(@RequestBody String payload){
        try {
            JSONObject jsonObject = new JSONObject(payload);
            long id = jsonObject.getInt("id");
            User user = userRepository.findById(id);
            if (jsonObject.has("name")){
                user.setName(jsonObject.getString("name"));
            }
            if (jsonObject.has("numero")){
                user.setNumero(jsonObject.getString("phone"));
            }
            if (jsonObject.has("photo")){
                user.setPhoto(jsonObject.getString("photo"));
            }
            if (jsonObject.has("pin")){
                user.setCodePin(jsonObject.getString("pin"));
            }
            if (jsonObject.has("solde")){
                user.setSolde(jsonObject.getInt("solde"));
            }
            userRepository.save(user);
            return new UserResponse(1,false,"user has been update",userRepository.findById(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new UserResponse(0,true,"fail to update user",null);
    }

    @GetMapping("/show")
    public Object getUser(@RequestParam("id")long id){
        return userRepository.findById(id);
    }
}
