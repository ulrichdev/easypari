package com.imedia.pari.Controllers;

import com.imedia.pari.Models.Response.BeerResponse;
import com.imedia.pari.Models.User;
import com.imedia.pari.Repository.UserRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class API {
    private UserRepository userRepository;

    @Autowired
    public API(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/beertransaction")
    public Object BeerPayer(@RequestBody String payload){

        try {
            JSONObject jsonObject = new JSONObject(payload);
            long id = jsonObject.getInt("id");
            String phone = jsonObject.getString("number");
            long amount = jsonObject.getInt("montant");

            User user = userRepository.findById(id);
            if (user.getSolde()>=amount){
                long result = user.getSolde() - amount;
                User user1 = userRepository.findByNumero(phone);
                long result2 = user1.getSolde() + amount;
                user.setSolde(result);
                user1.setSolde(result2);
                userRepository.save(user);
                userRepository.save(user1);

                return new BeerResponse(false,"Transaction has been successfully");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new BeerResponse(true,"Insuffisance credit");
    }

    @RequestMapping(value = "/wallettransaction")
    public Object WalletPayer(@RequestBody String payload){

        try {
            JSONObject jsonObject = new JSONObject(payload);
            long id = jsonObject.getInt("id");
            long amount = jsonObject.getInt("montant");

                User user = userRepository.findById(id);
                long result = user.getSolde()+amount;
                user.setSolde(result);
                userRepository.save(user);

                return new BeerResponse(false,"Transaction has been successfully");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new BeerResponse(true,"");
    }


}
