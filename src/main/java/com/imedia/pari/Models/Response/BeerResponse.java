package com.imedia.pari.Models.Response;

public class BeerResponse {

    private boolean error;
    private String message;

    public BeerResponse(boolean error, String message) {
        this.error = error;
        this.message = message;
    }
}
