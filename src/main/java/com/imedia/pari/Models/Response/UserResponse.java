package com.imedia.pari.Models.Response;

import com.imedia.pari.Models.User;

public class UserResponse {

    private int responsecode;
    private boolean error;
    private String message;
    private User user;

    public UserResponse(int responsecode, boolean error, String message, User user) {
        this.responsecode = responsecode;
        this.error = error;
        this.message = message;
        this.user = user;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
