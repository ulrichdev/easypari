package com.imedia.pari.Models;

import javax.persistence.*;

@Entity
@Table(name = "users", catalog = "pari")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String numero;
    private long solde;
    private String codePin;
    private String photo;

    public User() {
    }
    public User(String name, String numero, long solde, String codePin, String photo){
        this.name = name;
        this.numero = numero;
        this.solde = solde;
        this.codePin = codePin;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getSolde() {
        return solde;
    }

    public void setSolde(long solde) {
        this.solde = solde;
    }

    public String getCodePin() {
        return codePin;
    }

    public void setCodePin(String codePin) {
        this.codePin = codePin;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
