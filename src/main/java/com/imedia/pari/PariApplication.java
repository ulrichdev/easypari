package com.imedia.pari;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PariApplication {

	public static void main(String[] args) {
		SpringApplication.run(PariApplication.class, args);
	}

}
