package com.imedia.pari.Repository;

import com.imedia.pari.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

    User findByNumero(String numero);
    User findById(long id);
}
